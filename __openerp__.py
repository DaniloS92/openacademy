# -*- coding: utf-8 -*-
{
    'name': "Open Academy",

    'summary': """Manage Trainings""",

    'description': """
        Open Academy module for managing training:
        - training courses
        - training sessions
        - attendees registrations
    """,

    'author': "Danilo Sanchez",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Test',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'board', 'report_xlsx'],

    # always loaded
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'views/actions.xml',
        'views/menuitems.xml',
        'views/openacademy_view.xml',
        'views/partner_view.xml',
        'views/session_workflow.xml',
        'views/session_board.xml',
        'views/report_course.xml',
        'views/report_wizard_session.xml',
        'views/session_instructor_report.xml',
        'views/session_instructor_report_xls.xml',
        'wizard/sessioninstructor_report_view.xml',
        # 'reports.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}