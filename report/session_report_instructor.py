# -*- coding: utf-8 -*-
from openerp import api, models


class RerportInstructor(models.AbstractModel):
    _name = 'report.openacademy.report_wizard_session_view_inst'

    @api.multi
    def render_html(self, data=None):
        self.model = self.env.context.get('active_model')
        docs = self.env[self.model].browse(self.env.context.get('active_id'))
        if docs.session_filter_date:
            query = "select s.name, s.start_date, s.seats, s.duration, p.name as instructor, o.name as course " \
                    "from openacademy_session s " \
                    "inner join res_partner p on s.instructor_id = p.id " \
                    "inner join openacademy_course o on s.course_id = o.id " \
                    "where s.instructor_id in ({}) and s.active={} " \
                    "and s.start_date between '{}' and '{}'".format(
                        ",".join(map(lambda x: str(x), docs.instructor_ids.mapped('id'))),
                        docs.session_active,
                        docs.session_start_date_from,
                        docs.session_start_date_to)
        else:
            query = "select s.name, s.start_date, s.seats, s.duration, p.name as instructor, o.name as course " \
                    "from openacademy_session s " \
                    "inner join res_partner p on s.instructor_id = p.id " \
                    "inner join openacademy_course o on s.course_id = o.id " \
                    "where s.instructor_id in ({}) and s.active={}".format(
                    ",".join(map(lambda x: str(x), docs.instructor_ids.mapped('id'))),
                    docs.session_active)
        self.env.cr.execute(query)
        sessions = self.env.cr.dictfetchall()

        params = {
            'instructors': ', '.join(docs.instructor_ids.mapped('name')),
            'session_active': docs.session_active
        }
        docargs = {
            'doc_ids': self.ids,
            'doc_model': self.model,
            'docs': sessions,
            'params': params,
        }
        return self.env['report'].render("openacademy.report_wizard_session_view_inst",
                                         docargs)
