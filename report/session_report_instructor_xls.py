# -*- coding: utf-8 -*-
from openerp.addons.report_xlsx.report.report_xlsx import ReportXlsx


class SessionReportXlsx(ReportXlsx):

    def generate_xlsx_report(self, workbook, data, lines):

        sheet = workbook.add_worksheet('Sesiones')

        bold = workbook.add_format({'bold': 1})

        header_format = workbook.add_format({
            'bold': True,
            'border': 1,
            'align': 'center',
            'fg_color': '#D7E4BC',
        })
        merge_format = workbook.add_format({
            'bold': True,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter',
            'fg_color': '#D7E4BC',
        })

        if not data['session_filter_date']:
            sessions = self.env['openacademy.session'].search([
                ('instructor_id', 'in', data.get('instructor_ids')),
                ('active', '=', data.get('session_active'))
            ])
        else:
            sessions = self.env['openacademy.session'].search([
                '&', '&',
                ('instructor_id', 'in', data.get('instructor_ids')),
                ('active', '=', data.get('session_active')),
                '&',
                ('start_date', '>=', data.get('session_start_date_from')),
                ('start_date', '<=', data.get('session_start_date_to'))
            ])

        if sessions:
            sheet.merge_range('A1:F2', 'LISTA DE CLASES', merge_format)
            sheet.write('A3', 'Nombre', header_format)
            sheet.write('B3', 'Curso', header_format)
            sheet.write('C3', 'Instructor', header_format)
            sheet.write('D3', 'Fecha Inicio', header_format)
            sheet.write('E3', 'Duracion(Horas)', header_format)
            sheet.write('F3', '# Asientos', header_format)

            row = 3
            col = 0

            for session in sessions:
                sheet.write_string(row, col, session.name)
                sheet.write_string(row, col + 1, session.course_id.name)
                sheet.write_string(row, col + 2, session.instructor_id.name)
                sheet.write_string(row, col + 3, session.start_date)
                sheet.write_number(row, col + 4, session.duration)
                sheet.write_number(row, col + 5, session.seats)
                row += 1

            sheet.write_string(row, 0, 'Total Horas', bold)
            sheet.write_formula(row, 4, '=SUM(E4:E{})'.format(row))
        else:
            sheet.merge_range('A1:L23', 'The query did not give any results.', merge_format)


SessionReportXlsx('report.openacademy.report_wizard_session_xls.xlsx', 'openacademy.session_wizard')
