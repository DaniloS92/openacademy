# -*- coding: utf-8 -*-

from openerp import models, fields, api


class ReportSessionWizard(models.TransientModel):
    _name = 'openacademy.session_wizard'
    _description = 'Sessions instructor wizard'

    instructor_ids = fields.Many2many(
        'res.partner',
        string="Instructor",
        domain=[('instructor', '=', True)],
        required=True
    )
    session_active = fields.Boolean(
        string="Active Sessions",
        default=True
    )
    session_filter_date = fields.Boolean(
        string="Filter Date?",
        default=False,
        help="Search sessions for a range of start dates"
    )
    session_start_date_from = fields.Date(
        string="Start date from",
        required=False,
    )
    session_start_date_to = fields.Date(
        string="Start date to",
        required=False
    )

    @api.multi
    def check_report(self):
        data = {}
        data['form'] = self.read(['instructor_ids', 'session_active',
                                  'session_start_date_from', 'session_start_date_to'])[0]
        return self._print_report(data)

    def _print_report(self, data):
        data['form'].update(self.read(['instructor_ids', 'session_active',
                                       'session_start_date_from', 'session_start_date_to'])[0])
        return self.env['report'].get_action(self, "openacademy.report_wizard_session_view_inst", data=data)

    @api.multi
    def print_xls_report(self):
        self.ensure_one()
        data = self.read()[0]
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'openacademy.report_wizard_session_xls.xlsx',
            'report_type': 'xlsx',
            'datas': data,
        }
