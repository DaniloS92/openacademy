# -*- coding: utf-8 -*-
from openerp import fields, models

class Parent(models.Model):
    
    _inherit = 'res.partner'
    instructor = fields.Boolean("Instructor",default=True)
    session_ids = fields.Many2many('openacademy.session',
        string='Attended Sesions', 
        readonly=True
    )
