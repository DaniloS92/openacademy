# -*- coding: utf-8 -*-

from datetime import timedelta
from openerp import models, fields, api, exceptions
from openerp.exceptions import UserError


class Course(models.Model):
    _name = 'openacademy.course'
    
    name = fields.Char(string='name', required = True)
    description = fields.Text(string='description')
    responsable_id = fields.Many2one(
        'res.users',
        ondelete='set null',
        string='Responsable',
        index=True
    )
    session_ids = fields.One2many(
        'openacademy.session',
        'course_id',
        string="Sessions",
    )
    count_sessions = fields.Float(
        string='Percentage of sessions',
        compute='_count_sessions',
        sorted=True
    )
    limit_sessions = fields.Integer(
        string='Limit session per course'
    )

    @api.multi
    def _count_sessions(self):
        for record in self:
            try:
                record.count_sessions = (len(record.session_ids) * 100) / record.limit_sessions
            except Exception as e:
                record.count_sessions = 0
    
    @api.multi
    @api.onchange('limit_sessions','session_ids')
    def _validate_sessions(self):
        for record in self:
            if(len(self.session_ids) > self.limit_sessions):
                raise exceptions.ValidationError("Session limit exceeded!")

    @api.multi
    def copy(self, default=None):
        default = dict(default or {})

        copied_count = self.search_count(
            [('name', '=like', u"Copy of {}%".format(self.name))])
        if not copied_count:
            new_name = u"Copy of {}".format(self.name)
        else:
            new_name = u"Copy of {} ({})".format(self.name, copied_count)

        default['name'] = new_name
        return super(Course, self).copy(default)

    _sql_constraints = [
        ('_check_name_description',
         'CHECK (name != description)',
         'The name and description of the course can not equals'),
        ('_check_unique_name',
         'UNIQUE (name)',
         'The name of the course has to be unique')
    ]

class Session(models.Model):
    _name = 'openacademy.session'

    def _default_course(self):
        return self.env['openacademy.course'].browse(self._context.get('course_id', False))

    name = fields.Char(required=True)
    start_date = fields.Date(
    default=fields.Date.today,
    )
    duration = fields.Float(
        digits=(6, 2),
        help="Duration in days",
    )
    duration_in_hours = fields.Float(
        digits=(6, 2),
        string="Duration in hours",
        compute="_get_duration_in_hours",
        inverse="_set_duration_in_hours",
        store=True,
    )
    seats = fields.Integer(string="Number of seats")
    instructor_id = fields.Many2one(
        'res.partner',
        string="Instructor",
        domain=['|', ('instructor', '=', True),
                ('category_id.name', 'ilike', 'Teacher')
        ]
    )
    course_id = fields.Many2one(
        'openacademy.course',
        ondelete="cascade",
        string="Course",
        required=True,
        default=_default_course
    )
    attendee_ids = fields.Many2many(
        'res.partner',
        domain=[('company_type','=','person')],
        string="Attendees"
    )
    taken_seats = fields.Float(
        string='Taken Seats',
        compute='_taken_seats'
    )
    active = fields.Boolean(default=True)
    end_date = fields.Date(
        string="End Date",
        store=True,
        compute="_get_end_date",
        inverse="_set_end_date"
    )
    attendees_count = fields.Integer(
        string="Attendees count",
        compute="_get_attendees_count",
        store=True
    )
    color = fields.Integer()
    state = fields.Selection(
        [
            ('draft', 'Draft'),
            ('confirmed', 'Confirmed'),
            ('done', 'Done')
        ],
        default='draft'
    )

    @api.multi
    def action_draft(self):
        self.state = 'draft'
    
    @api.multi
    def action_confirm(self):
        self.state = 'confirmed'
    
    @api.multi
    def action_done(self):
        self.state = 'done'

    @api.depends('attendee_ids')
    def _get_attendees_count(self):
        for r in self:
            r.attendees_count = len(self.attendee_ids)

    @api.depends('attendee_ids', 'seats')
    def _taken_seats(self):
        for record in self:
            if not record.seats:
                record.taken_seats = 0.0
            else:
                record.taken_seats = 100.0 * len(record.attendee_ids) / record.seats

    @api.onchange('attendee_ids', 'seats')
    def _verify_valid_seats(self):
        for r in self:
            if len(r.attendee_ids) > r.seats:
                return {
                    'warning':{
                        'title': 'Too many attendees',
                        'message': "Increase seats or remove excess attendes",
                    }
                }
            if r.seats < 0:
                return {
                    'warning': {
                        'title': 'No negative vals',
                        'message': "No negative vals in seats",
                    }
                }

    @api.constrains('instructor_id', 'attendee_ids')
    def _check_instructor_not_in_attendees(self):
        for r in self:
            if r.instructor_id and r.instructor_id in r.attendee_ids:
                raise exceptions.ValidationError("A session's instructor can't be an attendee")
    
    @api.constrains('seats', 'attendee_ids')
    def _check_attenee_ids(self):
        for record in self:
            if record.seats < len(record.attendee_ids):
                raise exceptions.ValidationError("There is no seats avaliable in this session({})".format(record.name))

    @api.depends('start_date', 'duration')
    def _get_end_date(self):
        for r in self:
            if not (r.start_date and r.duration):
                r.end_date = r.start_date
                continue
            start = fields.Datetime.from_string(r.start_date)
            duration = timedelta(days=r.duration, seconds=-1)
            r.end_date = start + duration

    def _set_end_date(self):
        for r in self:
            if not (r.start_date and r.end_date):
                continue
            start_date = fields.Datetime.from_string(r.start_date)
            end_date = fields.Datetime.from_string(r.end_date)
            r.duration = (end_date - start_date).days + 1

    @api.depends('duration')
    def _get_duration_in_hours(self):
        for r in self:
            r.duration_in_hours = r.duration * 24

    def _set_duration_in_hours(self):
        for r in self:
            r.duration = r.duration_in_hours / 24
    
    @api.model
    def create(self, vals):
        if vals.get('course_id', False):
            course = self.env['openacademy.course'].browse(vals['course_id'])
            if (len(course.session_ids) + 1) > course.limit_sessions:
                raise UserError('Exceeded limit of sessions assigned to the course.')
        return super(Session, self).create(vals)