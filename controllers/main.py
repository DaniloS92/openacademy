# -*- coding: utf-8 -*-
from openerp import http
from openerp.http import request


class Main(http.Controller):

    @http.route('/openacademy/sessions', type='http', auth='none')
    def sesions(self):
        records = request.env['openacademy.session'].sudo().search([])
        result = '<html><body><table><tr><td>'
        result += '</td></tr><tr><td>'.join(records.mapped('name'))
        result += '</td></tr></table></body></html>'
        return result

    @http.route('/openacademy/partners/json', type='json', auth='none')
    def partners_json(self):
        records = request.env['res.partner'].sudo().search([])
        return [self._serializar_partners(partner) for partner in records]

    def _serializar_partners(self, model_partner):
        return {
            "id": model_partner.id,
            "name": model_partner.name,
            "create_date": model_partner.create_date,
            "is_supplier": model_partner.supplier
        }